#!/bin/bash


if [ $# -lt 5 ];then
	echo "Usage: $0 AMM_IP "
	echo "		power Blade1 BladeN Increment"
	echo "		bootseq Blade1 BladeN Increment"
	echo "		bootseq local Blade1 BladeN Increment"
	echo "		bootseq network Blade1 BladeN Increment"
	exit 1
fi

_amm_ip=$1


if [ "$2" == "power" ];then
	_b1=$3
	_bn=$4
	_inc=$5
	echo "Restarting blade [$_b1] to [$_bn]"
	for i in `seq $_b1 $_inc $_bn`;do
		ssh $_amm_ip power -cycle -T system:blade[$i]
		if [ $? -eq 0 ];then
			echo "Blade [$i] successfully restarted ..."
		else
			echo "Blade [$i] failed to restart ..."
		fi
	done
fi

if [ "$2" == "bootseq" ];then
	if [ $# -eq 5 ];then	
		_b1=$3
		_bn=$4
		_inc=$5
		echo "Checking boot sequence for blades [$_b1] to [$_bn] ..."
		for i in `seq $_b1 $_inc $_bn`;do
			ssh $_amm_ip bootseq -T system:blade[$i]
		done
	fi
	if [ $# -eq 6 ] && [ "$3" == "local" ];then
		_b1=$4
		_bn=$5
		_inc=$6
		echo "Setting boot sequest to local for blades [$_b1] to [$_bn]..."
		for i in `seq $_b1 $_inc $_bn`;do
			ssh $_amm_ip bootseq hd0 nw cd legacy -T system:blade[$i]
		done
	fi
	if [ $# -eq 6 ] && [ "$3" == "network" ];then
		_b1=$4
		_bn=$5
		_inc=$6		
		echo "Setting boot sequest to network for blades [$_b1] to [$_bn]..."
		for i in `seq $_b1 $_inc $_bn`;do
			ssh $_amm_ip bootseq nw hd0 cd legacy -T system:blade[$i]
		done
	fi
fi
